//
//  MoviesListViewController.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 5/20/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit
import TMDBSwift
import PKHUD
import FirebaseDatabase
import FirebaseAuth

class MoviesListViewController: UIViewController{
     var ref: DatabaseReference! = Database.database().reference()
    @IBOutlet var moviesTable: UITableView!
    var selectedGenre: GenresMDB = GenresMDB(results: "")
    var movies:[MovieMDB]=[]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = self.selectedGenre.name
        self.moviesTable.tableFooterView = UIView()
        self.LoadData()
    }

    func LoadData()->Void{
        
        
         HUD.show(.progress)
        
        switch self.selectedGenre.id {
        case -1:
           self.LoadFavorites()
            break
        case -2:
            MovieMDB.popular(page: 1) { (clientRet, movies) in
                self.movies = movies!
                
                self.moviesTable.reloadData()
                HUD.flash(.success, delay: 0)
            }
            break
        case -3:
            MovieMDB.toprated(page: 1) { (clientRet, movies) in
                self.movies = movies!
                
                self.moviesTable.reloadData()
                HUD.flash(.success, delay: 0)
            }
            break
        case -4:
            MovieMDB.upcoming(page: 1) { (clientRet, movies) in
                self.movies = movies!
                
                self.moviesTable.reloadData()
                HUD.flash(.success, delay: 0)
            }
            break
        case -5:
            MovieMDB.nowplaying(page: 1) { (clientRet, movies) in
                self.movies = movies!
                
                self.moviesTable.reloadData()
                HUD.flash(.success, delay: 0)
            }
            break
        default:
            GenresMDB.genre_movies(genreId: self.selectedGenre.id!, include_adult_movies: false, language: "en")
            { (clientRet, movies) in
                self.movies = movies!
                
                self.moviesTable.reloadData()
                HUD.flash(.success, delay: 0)
            }
        }
        
        
    }
    
    func LoadFavorites(){
        
        let user = Auth.auth().currentUser
        
        let refFavs = self.ref.child("users").child((user?.uid)!).child("Favorites").child("Movies")
        
        refFavs.observe(DataEventType.value, with: { (snapshot) in
            
            //if the reference have some values
            if snapshot.childrenCount > 0 {
                
                //clearing the list
                self.movies.removeAll()
                
                //iterating through all the values
                for fav in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let favObj = fav.value as? [String: AnyObject]
                    
                    //creating artist object with model and fetched values
                    let newFav = MovieMDB(results: "")
                    newFav.title = (favObj?["title"] as! String)
                    newFav.poster_path = (favObj?["poster_path"] as! String)
                    newFav.release_date = (favObj?["release_date"] as! String)
                    newFav.vote_average = Double(favObj?["vote_average"] as! String)
                    newFav.overview = (favObj?["description"] as! String)
                    //appending it to list
                    self.movies.append(newFav)
                    
                    
                }
                
            }
            self.moviesTable.reloadData()
            HUD.flash(.success, delay: 0)
        })
        
    }
   
    
    func GenerateImageByPoster(poster: String) -> UIImage{
        var poster_path = "https://image.tmdb.org/t/p/original"
        poster_path+=poster
        let url = URL(string: (poster_path))
        let data = try? Data(contentsOf: url!)
        return UIImage(data: data!)!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

   
    
}

extension MoviesListViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genCell = tableView.dequeueReusableCell(withIdentifier: "MoviesListCellId") as! MoviesListViewCell?
        
        genCell?.preservesSuperviewLayoutMargins = false
        genCell?.separatorInset = UIEdgeInsets.zero
        genCell?.layoutMargins = UIEdgeInsets.zero
        
        genCell?.movieTitle.text = self.movies[indexPath.row].title
        genCell?.movieImage.image = self.GenerateImageByPoster(poster: self.movies[indexPath.row].poster_path!)
        let release = self.movies[indexPath.row].release_date!.split(separator: "-")
        let releaseDate = "\(release[2])/\(release[1])/\(release[0])"
        genCell?.movieReleaseDate.text = "Release date: \(releaseDate)"
        genCell?.movieRate.text = "User score: \(self.movies[indexPath.row].vote_average ?? 0)"
        
        return genCell!
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let favs = UIContextualAction(style: .normal, title: "Add to favorites") { (action, view, completion) in
            
            HUD.show(.progress)
            
            let user = Auth.auth().currentUser
            
            let refFavs = self.ref.child("users").child((user?.uid)!).child("Favorites").child("Movies")
            
            let currentMovie = self.movies[indexPath.row]
            
            let artist = ["id":currentMovie.id,
                          "title": currentMovie.title! as String,
                          "release_date": currentMovie.release_date! as String,
                          "vote_average": String(format: "%.2f", currentMovie.vote_average!) as String,
                          "description": currentMovie.overview! as String,
                          "poster_path": currentMovie.poster_path! as String
                ] as [String : Any]
            
            refFavs.child(String(currentMovie.id)).setValue(artist)
            
            HUD.flash(.success)
            
            completion(true)
            
        }
        
        favs.backgroundColor = #colorLiteral(red: 0.04068163575, green: 0.8179406267, blue: 0.2222613719, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [favs])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delFav = UIContextualAction(style: .normal, title: "Remove from favorites") { (action, view, completion) in
          
            HUD.show(.progress)
            
            let user = Auth.auth().currentUser
            let refFavs = self.ref.child("users").child((user?.uid)!).child("Favorites").child("Movies")
            refFavs.child(String(self.movies[indexPath.row].id)).removeValue()
            
            completion(true)
            
            HUD.flash(.success)
            
            
        }
        
        delFav.backgroundColor = #colorLiteral(red: 1, green: 0.05808211098, blue: 0.00492186604, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [delFav])
        
    }
    
}

extension MoviesListViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let moviesDetailViewController = storyboard?.instantiateViewController(withIdentifier: "MovieDetailsId") as! MovieDetailsController
        moviesDetailViewController.selectedMovie = self.movies[indexPath.row]
        
        navigationController?.pushViewController(moviesDetailViewController, animated: true)
        
        
    }
    
}
