//
//  Series Tracker
//
//  Created by Andrei Funaru on 4/3/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit
import TMDBSwift
import PKHUD

class MoviesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    
    @IBOutlet weak var tableGenres: UITableView!
    
    var genres: [GenresMDB]=[]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableGenres.tableFooterView = UIView()
        
        self.LoadData()
    }
    
    func LoadData() -> Void {
        HUD.show(.progress)
        
        GenresMDB.genres(listType: .movie, language: "en") { (clientRet, genres) in
            self.genres = genres!
            
            let favs = GenresMDB(results: "")
            favs.id = -1
            favs.name = "Favorites"
            
            let popular = GenresMDB(results: "")
            popular.id = -2
            popular.name = "Popular"
            
            let topRated = GenresMDB(results: "")
            topRated.id = -3
            topRated.name = "Top rated"
            
            let upcoming = GenresMDB(results: "")
            upcoming.id = -4
            upcoming.name = "Upcoming"
            
            let nowPlaying = GenresMDB(results: "")
            nowPlaying.id = -5
            nowPlaying.name = "Now playing"
            
            self.genres.insert(nowPlaying, at: 0)
            self.genres.insert(upcoming, at: 0)
            self.genres.insert(topRated, at: 0)
            self.genres.insert(popular, at: 0)
            self.genres.insert(favs, at: 0)
            
            self.tableGenres.reloadData()
            HUD.flash(.success, delay: 0)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.genres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genCell = tableView.dequeueReusableCell(withIdentifier: "moviesGenresCell") as! MoviesGenresTableViewCell?
        
        guard ((genCell?.movieTitle.text = self.genres[indexPath.row].name) != nil) else {
            return genCell!
        }
        
        genCell?.preservesSuperviewLayoutMargins = false
        genCell?.separatorInset = UIEdgeInsets.zero
        genCell?.layoutMargins = UIEdgeInsets.zero
        
        if(indexPath.row % 2 == 1){
            genCell?.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        }else
        {
            genCell?.backgroundColor=UIColor.white
        }
        
        return genCell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let selectedGenres = self.genres[indexPath.row]
        
        let moviesListViewController = storyboard?.instantiateViewController(withIdentifier: "MoviesListId") as! MoviesListViewController
        moviesListViewController.selectedGenre = selectedGenres
        
        navigationController?.pushViewController(moviesListViewController, animated: true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   

}



