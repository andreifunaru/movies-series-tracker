//
//  MoviesListViewCell.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 5/20/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit

class MoviesListViewCell: UITableViewCell {

    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieReleaseDate: UILabel!
    @IBOutlet weak var movieRate: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
