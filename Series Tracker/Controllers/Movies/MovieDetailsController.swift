//
//  MovieDetailsController.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 5/23/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit
import TMDBSwift
import FirebaseDatabase
import FirebaseAuth

class MovieDetailsController: UIViewController {

    
    
    var selectedMovie: MovieMDB = MovieMDB(results: "")
    
    @IBOutlet weak var movieDetailPoster: UIImageView!
    @IBOutlet weak var movieDetailTitle: UILabel!
    @IBOutlet weak var movieDetailRelease: UILabel!
    @IBOutlet weak var movieDetailRate: UILabel!
    @IBOutlet weak var movieDetailDescription: UILabel!
    override func viewDidLoad() {
        
       
        
        super.viewDidLoad()
        
        self.movieDetailTitle.text = self.selectedMovie.title
        
        let release = self.selectedMovie.release_date!.split(separator: "-")
        let releaseDate = "\(release[2])/\(release[1])/\(release[0])"
        self.movieDetailRelease.text = "Release date: \(releaseDate)"
        self.movieDetailRate.text = "User rate: \(self.selectedMovie.vote_average ?? 0)"
        self.movieDetailDescription.text = self.selectedMovie.overview
        
        self.movieDetailPoster.image = self.GenerateImageByPoster(poster: self.selectedMovie.poster_path!)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func GenerateImageByPoster(poster: String) -> UIImage{
        var poster_path = "https://image.tmdb.org/t/p/original"
        poster_path+=poster
        let url = URL(string: (poster_path))
        let data = try? Data(contentsOf: url!)
        return UIImage(data: data!)!
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
