//
//  EditProfileViewController.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 5/27/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit
import MobileCoreServices
import PKHUD
import FirebaseStorage
import FirebaseAuth
import FirebaseDatabase

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var ref: DatabaseReference! = Database.database().reference()
    var delegate: ProfileViewController!
    
    @IBOutlet weak var editNameField: UITextField!
    @IBOutlet weak var imageEdit: UIImageView!
    
    var profileImage: String = ""
    var name: String = ""
    var changedImage: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.editNameField.text = self.name
        
        let url = URL(string: self.profileImage)
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        self.imageEdit.image = UIImage(data: data!)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func DismissEdit(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SaveEdit(_ sender: Any) {
        
        HUD.show(.progress)
        
        if self.changedImage == true {
            
            self.upload(self.imageEdit.image!) { (finished, imageUrl) in
                
                if finished == false {
                    
                    self.ShowError(title: "Error", message: "Error at uploading image to stoarge!", buttonText: "Close")
                    
                }else{
                    self.profileImage = imageUrl
                    
                    self.SaveChangesFirebase(username: self.editNameField.text!, imageUrl: imageUrl)
                    self.delegate.passData(Username: self.editNameField.text!, ImageUrl: self.profileImage)
                    
                    HUD.flash(.success)
                    self.dismiss(animated: true, completion: nil)
                    
                }
                
                
            }
            
        }
        else
        {
            self.SaveChangesFirebase(username: self.editNameField.text!, imageUrl: self.profileImage)
            self.delegate.passData(Username: self.editNameField.text!, ImageUrl: self.profileImage)
            HUD.flash(.success)
            self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
    func SaveChangesFirebase(username: String, imageUrl: String){
        
        let user = Auth.auth().currentUser
        self.ref.child("users").child((user?.uid)!).updateChildValues(["Username" : username,
                                                                       "ProfileImage": imageUrl])
        
    }
    
    func upload(_ image: UIImage,
                       completion: @escaping (_ hasFinished: Bool, _ url: String) -> Void) {
        
        let storage = Storage.storage()
        
        var data = Data()
        
        data = UIImageJPEGRepresentation(self.imageEdit.image!, 1.0)!
        
         let user = Auth.auth().currentUser
        
        let storageRef = storage.reference()
        
        let imageRef = storageRef.child("images/\(user!.uid)/profile.jpeg")
        
        _ = imageRef.putData(data, metadata: nil, completion: { (metadata,error ) in
            
            if error == nil {
                // return url
                completion(true, (metadata?.downloadURL()?.absoluteString)!)
            } else {
                completion(false, "")
            }
            
            
        })
    }
    
    @IBAction func SelectImage(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.OpenImagePicker(type: 1)
        })
        let saveAction = UIAlertAction(title: "Photo library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.OpenImagePicker(type: 0)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }
    
    func OpenImagePicker(type: Int){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        switch type {
        case 0:
             imagePicker.sourceType = .photoLibrary
        default:
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePicker.sourceType = .camera
            }else
            {
                imagePicker.sourceType = .photoLibrary
            }
        }
        
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: imagePicker.sourceType)!
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func ShowError(title: String, message: String, buttonText: String)->Void{
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension EditProfileViewController {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        
        if mediaType == (kUTTypeImage as  String){
            
            self.imageEdit.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            self.changedImage = true
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
}
