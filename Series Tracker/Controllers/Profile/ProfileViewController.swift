//
//  ProfileViewController.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 4/21/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit
import  FirebaseAuthUI
import FirebaseAuth
import FirebaseDatabase
import PKHUD
import TMDBSwift

class ProfileViewController: UIViewController {

    var ref: DatabaseReference! = Database.database().reference()
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var tableFavs: UITableView!
    
    var imgProf: String = ""
    var usr: String = ""
    var favoritesMovies: [MovieMDB] = []
    var favoritesSeries: [MovieMDB] = []
    var section: [[MovieMDB]] = []
    var loaded: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableFavs.tableFooterView = UIView()
        
        self.SetInfoProfile()
        self.LoadFavoritesMovies()
        self.LoadFavoritesSeries()
    }
    
    func SetInfoProfile(){
        
        HUD.show(.progress)
        
        let user = Auth.auth().currentUser
        
        self.ref.child("users").child((user?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            self.usr = value?["Username"] as? String ?? (user?.displayName)!
            
            self.userName.text = self.usr
            
            self.imgProf = value?["ProfileImage"] as? String ?? (user?.photoURL?.absoluteString)!
            let url = URL(string: self.imgProf)
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            self.profileImage.image = UIImage(data: data!)
            
            HUD.flash(.success)
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SignOut(_ sender: Any) {
           let authUI = FUIAuth.defaultAuthUI()
        
        do {
            try  authUI!.signOut()
            let authControllerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AuthControllerId")
            self.present(authControllerVC,animated: true)
        } catch {
            print(error)
        }
        
      
    }
    
    func passData(Username: String, ImageUrl: String){
        
        self.usr = Username
        
        self.userName.text = self.usr
        
        self.imgProf = ImageUrl
        let url1 = URL(string: self.imgProf)
        let data1 = try? Data(contentsOf: url1!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        self.profileImage.image = UIImage(data: data1!)
        
    }
    
    @IBAction func EditProfile(_ sender: Any) {
        let profileEdit = storyboard?.instantiateViewController(withIdentifier: "EditProfileId") as! EditProfileViewController
        profileEdit.delegate = self
        
        profileEdit.name = self.usr
        profileEdit.profileImage = self.imgProf
        
        self.present(profileEdit, animated: true, completion: nil)
        
        
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProfileViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.section[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Movies"
        default:
            return "Series"
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let favCell = tableView.dequeueReusableCell(withIdentifier: "FavoritesViewCellID") as! FavoritesViewCell?
        
        favCell?.preservesSuperviewLayoutMargins = false
        favCell?.separatorInset = UIEdgeInsets.zero
        favCell?.layoutMargins = UIEdgeInsets.zero
        
        favCell?.facTitle.text = self.section[indexPath.section][indexPath.row].title
        favCell?.favImage.image = self.GenerateImageByPoster(poster: self.section[indexPath.section][indexPath.row].poster_path!)
        let release = self.section[indexPath.section][indexPath.row].release_date!.split(separator: "-")
        let releaseDate = "\(release[2])/\(release[1])/\(release[0])"
        favCell?.favRelease.text = "Release date: \(releaseDate)"
        favCell?.favRate.text = "User score: \(self.section[indexPath.section][indexPath.row].vote_average ?? 0)"
        
        return favCell!
    }
    
    
    
    
    func LoadFavoritesMovies(){
        
        let user = Auth.auth().currentUser
        
        let refFavs = self.ref.child("users").child((user?.uid)!).child("Favorites").child("Movies")
        
        refFavs.observe(DataEventType.value, with: { (snapshot) in
            
            //if the reference have some values
            if snapshot.childrenCount > 0 {
                
                //clearing the list
                self.favoritesMovies.removeAll()
                
                //iterating through all the values
                for fav in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let favObj = fav.value as? [String: AnyObject]
                    
                    //creating artist object with model and fetched values
                    let newFav = MovieMDB(results: "")
                    newFav.title = (favObj?["title"] as! String)
                    newFav.poster_path = (favObj?["poster_path"] as! String)
                    newFav.release_date = (favObj?["release_date"] as! String)
                    newFav.vote_average = Double(favObj?["vote_average"] as! String)
                    //appending it to list
                    self.favoritesMovies.append(newFav)
                    
                
                }
                
                if(self.loaded==true){
                    self.section = [self.favoritesMovies, self.favoritesSeries]
                    self.tableFavs.reloadData()
                }else
                {
                    self.loaded=true
                }
                
            }
        })
        
    }
  
    func LoadFavoritesSeries(){
        
        let user = Auth.auth().currentUser
        
        let refFavs = self.ref.child("users").child((user?.uid)!).child("Favorites").child("Series")
        
        refFavs.observe(DataEventType.value, with: { (snapshot) in
            
            //if the reference have some values
            if snapshot.childrenCount > 0 {
                
                //clearing the list
                self.favoritesSeries.removeAll()
                
                //iterating through all the values
                for fav in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let favObj = fav.value as? [String: AnyObject]
                    
                    //creating artist object with model and fetched values
                    let newFav = MovieMDB(results: "")
                    newFav.title = (favObj?["title"] as! String)
                    newFav.poster_path = (favObj?["poster_path"] as! String)
                    newFav.release_date = (favObj?["release_date"] as! String)
                    newFav.vote_average = Double(favObj?["vote_average"] as! String)
                    //appending it to list
                    self.favoritesSeries.append(newFav)
                    
                    
                }
                
                if(self.loaded==true){
                    self.section = [self.favoritesMovies, self.favoritesSeries]
                    self.tableFavs.reloadData()
                }else
                {
                    self.loaded=true
                }
                
            }
        })
        
    }
    
    func GenerateImageByPoster(poster: String) -> UIImage{
        var poster_path = "https://image.tmdb.org/t/p/original"
        poster_path+=poster
        let url = URL(string: (poster_path))
        let data = try? Data(contentsOf: url!)
        return UIImage(data: data!)!
    }
    
    
}
