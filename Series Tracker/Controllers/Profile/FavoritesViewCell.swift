//
//  FavoritesViewCell.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 5/29/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit

class FavoritesViewCell: UITableViewCell {

    @IBOutlet weak var favImage: UIImageView!
    @IBOutlet weak var facTitle: UILabel!
    @IBOutlet weak var favRelease: UILabel!
    @IBOutlet weak var favRate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
