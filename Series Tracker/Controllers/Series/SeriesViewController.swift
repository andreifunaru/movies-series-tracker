//
//  SeriesViewController.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 4/3/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit
import TMDBSwift
import PKHUD

class SeriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableGenres: UITableView!
    var genres: [GenresMDB] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableGenres.tableFooterView = UIView()
        
        HUD.show(.progress)
        
            
            let favs = GenresMDB(results: "")
            favs.id = -1
            favs.name = "Favorites"
            
            let popular = GenresMDB(results: "")
            popular.id = -2
            popular.name = "Popular"
            
            let topRated = GenresMDB(results: "")
            topRated.id = -3
            topRated.name = "Top rated"
            
            let onTV = GenresMDB(results: "")
            onTV.id = -4
            onTV.name = "On TV"
            
            let airingToday = GenresMDB(results: "")
            airingToday.id = -5
            airingToday.name = "Airing today"
        
            self.genres.insert(airingToday, at: 0)
            self.genres.insert(onTV, at: 0)
            self.genres.insert(topRated, at: 0)
            self.genres.insert(popular, at: 0)
            self.genres.insert(favs, at: 0)
            
            self.tableGenres.reloadData()
            HUD.flash(.success, delay: 0)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.genres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genCell = tableView.dequeueReusableCell(withIdentifier: "seriesGenreCell") as! SeriesGenreViewCell?
        
        guard ((genCell?.seriesTitle.text = self.genres[indexPath.row].name) != nil) else {
            return genCell!
        }
        
        genCell?.preservesSuperviewLayoutMargins = false
        genCell?.separatorInset = UIEdgeInsets.zero
        genCell?.layoutMargins = UIEdgeInsets.zero
        
        if(indexPath.row % 2 == 1){
            genCell?.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        }else
        {
            genCell?.backgroundColor=UIColor.white
        }
        
        return genCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let selectedGenres = self.genres[indexPath.row]
        
        let tvListViewController = storyboard?.instantiateViewController(withIdentifier: "SeriesListId") as! SeriesListViewController
        tvListViewController.selectedGenre = selectedGenres
        
        navigationController?.pushViewController(tvListViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

