//
//  SeriesDetailViewController.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 5/30/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit
import TMDBSwift

class SeriesDetailViewController: UIViewController {

    var selectedSeries: TVMDB = TVMDB(results: "")
    
    @IBOutlet weak var tvImage: UIImageView!
    @IBOutlet weak var tvTitle: UILabel!
    @IBOutlet weak var tvRelease: UILabel!
    @IBOutlet weak var tvRate: UILabel!
    @IBOutlet weak var tvDescription: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tvTitle.text = self.selectedSeries.name
        self.tvImage.image = self.GenerateImageByPoster(poster: self.selectedSeries.poster_path!)
        let release = self.selectedSeries.first_air_date!.split(separator: "-")
        let releaseDate = "\(release[2])/\(release[1])/\(release[0])"
        self.tvRelease.text = "Release date: \(releaseDate)"
        self.tvRate.text = "User score: \(self.selectedSeries.vote_average ?? 0)"
        self.tvDescription.text = self.selectedSeries.overview
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    func GenerateImageByPoster(poster: String) -> UIImage{
        var poster_path = "https://image.tmdb.org/t/p/original"
        poster_path+=poster
        let url = URL(string: (poster_path))
        let data = try? Data(contentsOf: url!)
        return UIImage(data: data!)!
    }

}
