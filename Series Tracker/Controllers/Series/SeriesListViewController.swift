//
//  SeriesListViewController.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 5/29/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit
import TMDBSwift
import PKHUD
import FirebaseDatabase
import FirebaseAuth

class SeriesListViewController: UIViewController {
    var ref: DatabaseReference! = Database.database().reference()
    @IBOutlet var seriesTable: UITableView!
    var selectedGenre: GenresMDB = GenresMDB(results: "")
    var series:[TVMDB]=[]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = self.selectedGenre.name
        self.seriesTable.tableFooterView = UIView()
        
        self.LoadData()
    }

    func LoadData()->Void{
        
        
        HUD.show(.progress)
        
        switch self.selectedGenre.id {
        case -1:
            self.LoadFavorites()
            break
        case -2:
            TVMDB.popular(page: 1, language: "en") { (clientRet, series) in
                self.series = series!
                
                self.seriesTable.reloadData()
                HUD.flash(.success)
            }
            break
        case -3:
            TVMDB.toprated(page: 1, language: "en") { (clientRet, series) in
               self.series = series!
                
                self.seriesTable.reloadData()
                HUD.flash(.success)
            }
            break
        case -4:
            TVMDB.ontheair(page: 1, language: "en") { (clientRet, series) in
                  self.series = series!
                
                self.seriesTable.reloadData()
                HUD.flash(.success)
            }
            break
        case -5:
            TVMDB.airingtoday(page: 1, language: "en", timezone: nil) { (clientRet, series) in
              self.series = series!
                
                self.seriesTable.reloadData()
                HUD.flash(.success)
            }
            break
        default:
            print("Hei")
        }
        
    }
    
    func LoadFavorites(){
        
        let user = Auth.auth().currentUser
        
        let refFavs = self.ref.child("users").child((user?.uid)!).child("Favorites").child("Series")
        
        refFavs.observe(DataEventType.value, with: { (snapshot) in
            
            //if the reference have some values
            if snapshot.childrenCount > 0 {
                
                //clearing the list
                self.series.removeAll()
                
                //iterating through all the values
                for fav in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let favObj = fav.value as? [String: AnyObject]
                    
                    //creating artist object with model and fetched values
                    let newFav = TVMDB(results: "")
                    newFav.name = (favObj?["title"] as! String)
                    newFav.poster_path = (favObj?["poster_path"] as! String)
                    newFav.first_air_date = (favObj?["release_date"] as! String)
                    newFav.vote_average = Double(favObj?["vote_average"] as! String)
                    newFav.overview = (favObj?["description"] as! String)
                    //appending it to list
                    self.series.append(newFav)
                    
                    
                }
                
            }
            self.seriesTable.reloadData()
            HUD.flash(.success, delay: 0)
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension SeriesListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.series.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tvCell = tableView.dequeueReusableCell(withIdentifier: "SeriesListViewCellId") as! SeriesListViewCell?
        
        tvCell?.preservesSuperviewLayoutMargins = false
        tvCell?.separatorInset = UIEdgeInsets.zero
        tvCell?.layoutMargins = UIEdgeInsets.zero
        
        tvCell?.tvTitle.text = self.series[indexPath.row].name
        tvCell?.tvImage.image = self.GenerateImageByPoster(poster: self.series[indexPath.row].poster_path!)
        let release = self.series[indexPath.row].first_air_date!.split(separator: "-")
        let releaseDate = "\(release[2])/\(release[1])/\(release[0])"
        tvCell?.tvRelease.text = "Release date: \(releaseDate)"
        tvCell?.tvRate.text = "User score: \(self.series[indexPath.row].vote_average ?? 0)"
        
        return tvCell!
    }
    
    func GenerateImageByPoster(poster: String) -> UIImage{
        var poster_path = "https://image.tmdb.org/t/p/original"
        poster_path+=poster
        let url = URL(string: (poster_path))
        let data = try? Data(contentsOf: url!)
        return UIImage(data: data!)!
    }
    
}

extension SeriesListViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let favs = UIContextualAction(style: .normal, title: "Add to favorites") { (action, view, completion) in
            
            HUD.show(.progress)
            
            let user = Auth.auth().currentUser
            
            let refFavs = self.ref.child("users").child((user?.uid)!).child("Favorites").child("Series")
            
            let currentTV = self.series[indexPath.row]
            
            let tv = ["id":currentTV.id,
                          "title": currentTV.name! as String,
                          "release_date": currentTV.first_air_date! as String,
                          "vote_average": String(format: "%.2f", currentTV.vote_average!) as String,
                          "description": currentTV.overview! as String,
                          "poster_path": currentTV.poster_path! as String
                ] as [String : Any]
            
            refFavs.child(String(currentTV.id)).setValue(tv)
            
            HUD.flash(.success)
            
            completion(true)
            
        }
        
        favs.backgroundColor = #colorLiteral(red: 0.04068163575, green: 0.8179406267, blue: 0.2222613719, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [favs])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delFav = UIContextualAction(style: .normal, title: "Remove from favorites") { (action, view, completion) in
            
            HUD.show(.progress)
            
            let user = Auth.auth().currentUser
            let refFavs = self.ref.child("users").child((user?.uid)!).child("Favorites").child("Series")
            refFavs.child(String(self.series[indexPath.row].id)).removeValue()
            
            completion(true)
            
            HUD.flash(.success)
            
            
        }
        
        delFav.backgroundColor = #colorLiteral(red: 1, green: 0.05808211098, blue: 0.00492186604, alpha: 1)
        
        return UISwipeActionsConfiguration(actions: [delFav])
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let seriesDetailViewController = storyboard?.instantiateViewController(withIdentifier: "SeriesDetailId") as! SeriesDetailViewController
        seriesDetailViewController.selectedSeries = self.series[indexPath.row]
        
        navigationController?.pushViewController(seriesDetailViewController, animated: true)
        
    }
    
}
