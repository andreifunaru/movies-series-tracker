//
//  SeriesListViewCell.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 5/29/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit

class SeriesListViewCell: UITableViewCell {

    @IBOutlet weak var tvTitle: UILabel!
    @IBOutlet weak var tvImage: UIImageView!
    @IBOutlet weak var tvRelease: UILabel!
    @IBOutlet weak var tvRate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
