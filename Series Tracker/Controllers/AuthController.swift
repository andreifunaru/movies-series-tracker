//
//  AuthController.swift
//  Series Tracker
//
//  Created by Andrei Funaru on 4/21/18.
//  Copyright © 2018 Andrei Funaru. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAuthUI
import FirebaseGoogleAuthUI
import FirebaseFacebookAuthUI

class AuthController: UIViewController, FUIAuthDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        checkLoggedIn()
    }

    func checkLoggedIn() {
        Auth.auth().addStateDidChangeListener { auth, user in
            if user != nil {
                
                self.redirectToTabBarController(index: 2)
                
            } else {
                // No user is signed in.
                self.login()
            }
        }
    }

    func login() {
        let authUI = FUIAuth.defaultAuthUI()
        authUI?.delegate = self
        authUI?.providers = [FUIGoogleAuth()]
        let authViewController = authUI?.authViewController()
        self.present(authViewController!, animated: true, completion: nil)
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        
        if error != nil {
            login()
        }
        else{
           redirectToTabBarController(index: 2)
        }
        
    }
    
    func redirectToTabBarController(index: Int){
        var idx = index
        let tabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarId") as! UITabBarController
        
        if (idx < 0 || 2 < idx){
            idx = 2
        }
        
        tabBarController.selectedIndex = idx
        self.present(tabBarController,animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
